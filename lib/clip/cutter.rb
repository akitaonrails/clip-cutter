# frozen_string_literal: true

require_relative "version"
require_relative "srt_entry"
require_relative "srt_file"
require "time"
require "open3"

module Clip
  class Error < StandardError; end

  # Cut clips from a video file
  # based on the subtitle file
  # video_path: path to the video file
  # srt_path: path to the subtitle file
  class Cutter
    attr_reader :video_path

    # doc: http://www.tcax.org/docs/ass-specs.htm
    DEFAULT_SRT_STYLES = { "Fontname": "Consolas",
                           "Fontsize": "14",
                           "Alignment": "2", # 1=Left, 2=Centered, 3=Right. Add 4 to the value for a "Toptitle". Add 8 to the value for a "Midtitle".
                           "PrimaryColour": "&H03fcff", # ALPHA BLUE GREEN RED
                           "OutlineColour": "&H000000",
                           "BackColour": "&H3D3D3D",
                           "BorderStyle": "1", # outline and shadow options only worh with style 1, 3 won't work
                           "Bold": "1",
                           "Outline": "1.5",
                           "Spacing": "1",
                           "Shadow": "0",
                           "MarginV": "50",
                           "MarginL": "10",
                           "MarginR": "20",
                           "PlayRexX": "600", # this is most important setting for wrapping
                           "WrapStyle": "0" }.freeze

    DEFAULT_DRAWTEXT = { "text": "AKITANDO",
                         "fontfile": "spec/fixtures/PermanentMarker-Regular.ttf",
                         "fontcolor": "white",
                         "fontsize": "60",
                         "line_spacing": "30",
                         "x": "(w-text_w)/2",
                         "y": "(h-text_h)/2",
                         "box": "1",
                         "boxcolor": "red@0.5",
                         "boxborderw": "10",
                         "shadowcolor": "black@0.5",
                         "shadowx": "2",
                         "shadowy": "2",
                         "enable": "'between(t,0,5)'" }.freeze

    def self.joinlist(list, separator = ":")
      list.map { |key, value| "#{key}=#{value}" }.join(separator)
    end

    def initialize(video_path, srt_path, speedup: 0.9, left_shift: 15, srt_styles: {}, text_styles: {}, rnd_color: true)
      raise Error, "Video file not found" unless File.exist?(video_path)
      raise Error, "Subtitle file not found" unless File.exist?(srt_path)

      @video_path = video_path
      @srt_file = SRTFile.new(srt_path)
      @rnd_color = rnd_color
      @speedup = speedup

      @left_shift = left_shift
      @styles = DEFAULT_SRT_STYLES.merge(srt_styles)
      text_styles = { "boxcolor": "#{random_color}@0.5" } if text_styles.empty? && rnd_color
      @drawtext = DEFAULT_DRAWTEXT.merge(text_styles)
    end

    def cut_clip(quote, output_path)
      opt = {}
      opt[:start_time] = @srt_file.find(quote, :start_time)
      opt[:end_time] = @srt_file.find(quote, :end_time)
      raise Error, "Clip not found" unless opt[:start_time] && opt[:end_time]

      # detect face to centralize the crop
      opt[:rect_width], opt[:rect_height], opt[:cx], opt[:cy] = detect_face

      # account for TikTok right side toolbar, so shift left
      opt[:cx] += @left_shift

      # extract subtitle file
      extracted = @srt_file.extract(quote)
      extract_temp_srt(extracted, "/tmp/extracted.srt")

      # fix overlapping subtitles
      # using a Go tool found in this Gist:
      # wget https://gist.github.com/nimatrueway/4589700f49c691e5413c5b2df4d02f4f/raw/a3cbf48edd6ad0377b158e1455a702895e17f2dd/subtitle-overlap-fixer.go
      `bin/subtitle-overlap-fixer "/tmp/extracted.srt"`

      # cut the clip
      output_path = output_path.gsub(" ", "-")
      tmp_output_path = change_filename(output_path, "tmp")
      tmp_output_path2 = change_filename(output_path, "tmp2")

      if process(ffmpeg_cut_crop(opt, @video_path, tmp_output_path), "Cutting and cropping clip")
        if process(ffmpeg_subtitles(tmp_output_path, tmp_output_path2), "Embedding Subtitle and Drawing Title Card")
          process(ffmpeg_speedup(tmp_output_path2, output_path), "Accelerating the video")
        end
      end

      # clean up
      #cleanup "/tmp/extracted.srt"
      cleanup tmp_output_path
      cleanup tmp_output_path2
    end

    private

    def process(command, log_message)
      puts "#{log_message}... #{command}"
      Kernel.system(command)
    end

    def cleanup(file)
      File.delete(file) if File.exist?(file)
    end

    def ffmpeg_cut_crop(opt, video_path, output_path)
      <<~CMD.strip
        ffmpeg -ss #{opt[:start_time]} -to #{opt[:end_time]} -i "#{video_path}" \
        -filter:v "crop=#{opt[:rect_width]}:#{opt[:rect_height]}:#{opt[:cx]}:#{opt[:cy]}" \
        -y "#{output_path}"
      CMD
    end

    def ffmpeg_subtitles(input_path, output_path)
      <<~CMD.strip
        ffmpeg -i #{input_path} \
        -vf "drawtext=#{Cutter.joinlist(@drawtext)},subtitles=/tmp/extracted.srt:force_style='#{Cutter.joinlist(@styles, ",")}'" \
        -c:a copy -y #{output_path}
      CMD
    end

    def ffmpeg_speedup(input_path, output_path)
      atempo = 1.0 / @speedup
      <<~CMD.strip
        ffmpeg -i #{input_path} \
        -filter_complex "[0:v]setpts=#{@speedup}*PTS[v];[0:a]atempo=#{atempo}[a]" \
        -map "[v]" -map "[a]" \
        -y #{output_path}
      CMD
    end

    def change_filename(file_path, suffix)
      file_ext = File.extname(file_path)
      file_name = File.basename(file_path, file_ext)
      output_fir = File.dirname(file_path)
      File.join(output_fir, "#{file_name}_#{suffix}#{file_ext}")
    end

    def random_color
      if @rnd_color
        %w[red green blue magenta darkgray purple violet pink orange].sample
      else
        "purple"
      end
    end

    def extract_temp_srt(extracted, extracted_file = "/tmp/subtitle.srt")
      File.delete(extracted_file) if File.exist?(extracted_file)
      File.open(extracted_file, "w") do |f|
        f.write(extracted.map(&:to_s).join(" "))
      end
    end

    def detect_face
      python_command = "python lib/face_detector.py \"#{@video_path}\""
      output, _, status = Open3.capture3(python_command)
      return false unless status
      return false if output.include?("No faces found")

      # calculate the crop dimensions
      output.split(",").map(&:to_i)
    end
  end
end
