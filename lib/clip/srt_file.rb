# frozen_string_literal: true

module Clip
  class SRTFile
    def initialize(srt_path)
      @srt_path = srt_path
      @srt_entries = parse_srt
      @srt_entries_reverse = @srt_entries.reverse # caching reverse list to avoid unnecessary processing
    end

    def parse_srt
      srt_entries = []
      File.open(@srt_path, "r") do |f|
        srt_entry = SRTEntry.new
        state = :number
        f.each_line do |line|
          case state
          when :number
            srt_entry.number = line.strip.to_i
            state = :time
          when :time
            srt_entry.parse_time(line)
            state = :text
          when :text
            if line.strip.empty?
              srt_entries << srt_entry
              srt_entry = SRTEntry.new
              state = :number
            else
              srt_entry.parse_text(line)
            end
          end
        end
        srt_entries << srt_entry if srt_entry&.number
      end
      srt_entries
    end

    def find_index(quote, time = :start_time)
      raise Clip::Error, "Invalid time argument, must be either :start_time or :end_time" unless [:start_time, :end_time].include?(time)

      quote = quote.downcase
      list, callback = case time
                       when :start_time
                         [@srt_entries, SRTEntry::SHIFT_LEFT]
                       when :end_time
                         [@srt_entries_reverse, SRTEntry::SHIFT_RIGHT]
                       end
      list.each_with_index do |entry, index|
        result_entry = process_entry(entry, quote, callback)
        # if found entry and the next entry is also in the quote, then it's a match
        next_text = list[index + 1]&.text&.downcase
        return index if result_entry && quote.include?(next_text)
      end
      return nil
    end

    def find(quote, time)
      index = find_index(quote, time)
      return nil unless index

      result = case time
               when :start_time
                 @srt_entries[index]
               when :end_time
                 @srt_entries_reverse[index]
               end
      result&.send(time)&.sub(",", ".")
    end

    def extract(quote, time = "00:00:00,000")
      start_time = find_index(quote, :start_time)
      end_time = @srt_entries.length - find_index(quote, :end_time) - 1

      raise Clip::Error, "Quote not found" unless start_time && end_time

      srt_entries = @srt_entries[start_time..(end_time + 1)]

      elapsed = srt_entries.first.elapsed_time(time)
      srt_entries.each do |entry|
        entry.shift_time(elapsed)
      end
      start_index = srt_entries.first.number
      srt_entries.each do |entry|
        entry.number -= start_index - 1
      end
      srt_entries
    end

    private

    def process_entry(entry, quote, shifter)
      entry_text = entry.text.downcase.strip
      while entry_text.include?(" ")
        return entry if quote.include?(entry_text)

        # remove word from the end and try to compare again
        # for the case of the entry having a partial match with the quote
        entry_text = shifter.call(entry_text)
      end
    end
  end
end
