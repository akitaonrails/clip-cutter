# frozen_string_literal: true
require "time"
module Clip
  class Error < StandardError; end

  # represents a single parsed subtitle entry
  class SRTEntry
    TIME_FORMAT = "%H:%M:%S,%L"
    SHIFT_LEFT  = ->(entry_text) { entry_text.split(" ")[1..].join(" ") }
    SHIFT_RIGHT = ->(entry_text) { entry_text.split(" ")[0..-2].join(" ") }

    attr_accessor :number, :text
    attr_writer :start_time, :end_time

    def parse_time(line)
      start_time, end_time = line.strip.split(" --> ")
      @start_time = Time.strptime(start_time, TIME_FORMAT)
      @end_time = Time.strptime(end_time, TIME_FORMAT)
    end

    def start_time
      @start_time.strftime(TIME_FORMAT)
    end

    def end_time
      @end_time.strftime(TIME_FORMAT)
    end

    def parse_text(line)
      self.text ||= ""
      line = line.strip.gsub(/\u00A0/, "")
      if self.text.empty?
        self.text = line
      else
        self.text += " #{line}"
      end
    end

    def elapsed_time(intro_time)
      if intro_time.is_a?(String)
        intro_time = Time.strptime(intro_time, TIME_FORMAT)
      end
      (@start_time - intro_time).to_f - 0.0001
    end

    def shift_time(elapsed)
      @start_time -= elapsed
      @end_time -= elapsed
    end

    def shift_time_to(intro_time)
      elapsed = elapsed_time(intro_time)
      shift_time(elapsed)
    end

    def to_s
      "#{number}\n#{start_time} --> #{end_time}\n#{text}\n"
    end
  end
end
