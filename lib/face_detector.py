import cv2
import sys

cap = cv2.VideoCapture(sys.argv[1])
draw_output = False
if len(sys.argv) > 2 and sys.argv[2] == 'draw_output':
    draw_output = True

total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
start_frame = 60 # rough estimate, 2 seconds ahead in the video
frame_num = start_frame
face_found = False

while frame_num < total_frames:
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_num)
    ret, frame = cap.read()
    if not ret:
        break

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier('/usr/share/opencv4/haarcascades/haarcascade_frontalface_default.xml')
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5)

    if len(faces) > 0:
        # Set the face_found flag to true
        face_found = True

        # Get the largest face
        (x, y, w, h) = sorted(faces, key=lambda x: x[2] * x[3], reverse=True)[0]

        # Calculate the center of the face
        cx = x + w // 2
        cy = y + h // 2

        # Define the rectangle dimensions
        rect_width = int(frame.shape[0] / 16 * 9)
        rect_height = frame.shape[0]
        rect_x = cx - rect_width // 2
        rect_y = 0

        # Draw the rectangle on the frame
        if draw_output:
          cv2.rectangle(frame, (rect_x, rect_y), (rect_x + rect_width, rect_y + rect_height), (0, 255, 0), 2)

        # Export the frame as png
        # cv2.imwrite('output.png', frame)

        # Print the coordinates of the center of the face
        print("{}, {}, {}, {}".format(rect_width, rect_height, rect_x, rect_y))

        # Exit the loop
        break

    frame_num += 1

if not face_found:
    print("No face found")

cap.release()
cv2.destroyAllWindows()

