# frozen_string_literal: true

RSpec.describe Batch::Excel do
  let(:excel) { Batch::Excel.new("spec/fixtures/quotes.xlsx", output: "/tmp") }

  describe "processing quotes" do
    let(:result) { excel.process_quotes(true) }
    before(:each) do
      allow(Time).to receive(:now).and_return(Time.parse("2018-01-01 12:00:00"))
    end
    context "first processed entry" do
      subject(:first_entry) { result.first.first }
      it { expect(first_entry[:input_file]).to eq("/mnt/terachad/Akitando Backup 1/Episode 0032.mp4") }
      it { expect(first_entry[:srt_file]).to eq("/mnt/terachad/Akitando Backup 1/captions/Episode 0032.srt") }
      it { expect(first_entry[:output_file]).to eq("/tmp/Episode 0032-12-00-00.mp4") }
      it { expect(first_entry[:quote]).to start_with("Já que eu tenho falado sobre") }
    end
    context "last processed entry" do
      subject(:last_entry) { result.first.last }
      it { expect(last_entry[:input_file]).to eq("/mnt/terachad/Akitando Backup 1/Episode 0032.mp4") }
      it { expect(last_entry[:srt_file]).to eq("/mnt/terachad/Akitando Backup 1/captions/Episode 0032.srt") }
      it { expect(last_entry[:output_file]).to eq("/tmp/Episode 0032-12-00-00.mp4") }
      it { expect(last_entry[:quote]).to start_with("Se você parar pra entender o básico sobre como o cérebro") }
    end
    context "first error entry" do
      subject(:first_error) { result.last.first }
      it { expect(first_error[:input_file]).to eq("/mnt/terachad/Akitando Backup 1/fake.mov") }
    end
  end
end
