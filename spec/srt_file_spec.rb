# frozen_string_literal: true

RSpec.describe Clip::SRTFile do
  context "Episode 0032.srt" do
    let(:srt_file) { Clip::SRTFile.new("spec/fixtures/Episode 0032.srt") }

    context "SRT parsed entry" do
      subject(:entry) { srt_file.parse_srt[0] }
      it { expect(entry.number).to eql(1) }
      it { expect(entry.start_time).to eql("00:00:00,000") }
      it { expect(entry.end_time).to eql("00:00:05,430") }
      it { expect(entry.text).to eql("olá pessoal fábio akita já que eu tenho") }
      it { expect(entry.to_s).to eql("1\n00:00:00,000 --> 00:00:05,430\nolá pessoal fábio akita já que eu tenho\n") }
    end

    # example snipper from SRT file:
    # 9
    # 00:00:20,100 --> 00:00:25,769
    # vamos dizer o óbvio para com as
    #
    # 10
    # 00:00:22,890 --> 00:00:27,840
    # justificativas qualquer razão que você
    #
    # 11
    # 00:00:25,769 --> 00:00:32,130
    # está usando para não evoluir no inglês
    #
    # o problema: não necessariamente o quote vai bater com o começo e fim exato dessas linhas,
    # mas precisa encontrar do mesmo jeito
    describe "finding quotes" do
      context "using the full lines of the subtitle" do
        subject(:quote) { "vamos dizer o óbvio para com as justificativas qualquer razão que você está usando para não evoluir no inglês" }
        it { expect(srt_file.find(quote, :start_time)).to eql("00:00:20.100") }
        it { expect(srt_file.find(quote, :end_time)).to eql("00:00:32.130") }
      end

      context "using partial lines of the subtitle" do
        subject(:quote) { "o óbvio para com as justificativas qualquer razão que você está usando" }
        it { expect(srt_file.find(quote, :start_time)).to eql("00:00:20.100") }
        it { expect(srt_file.find(quote, :end_time)).to eql("00:00:32.130") }
      end

      context "subset" do
        subject(:quote) { "o óbvio para com as justificativas qualquer razão que você está usando" }
        let(:extracted) { srt_file.extract(quote) }
        context "normalized" do
          it { expect(extracted.first.start_time).to eql("00:00:00,000") }
          it { expect(extracted.last.start_time).to eql("00:00:07,740") }
        end
      end
    end
  end

  context "Episode 0138.srt" do
    let(:srt_file) { Clip::SRTFile.new("spec/fixtures/Episode 0138.srt") }

    # 19
    # 00:01:40,860 --> 00:01:47,160
    # Mas vamos lá. O período entre 1996 até 2002 
    # foi provavelmente quando eu mais aprendi  

    # 20
    # 00:01:47,160 --> 00:01:51,780
    # tecnologias que não são mais usadas hoje em 
    # dia. A idéia é tanto um pouco de nostalgia  

    # 39
    # 00:03:28,440 --> 00:03:32,940
    # da Engenharia Elétrica. Eu fazia Ciência da 
    # Computação e nessa época já quase não tinha  

    # 40
    # 00:03:32,940 --> 00:03:37,680
    # nenhum amigo no meu curso mesmo.
    # Todo dia eu ficava deslumbrado com as novidades  
    describe "finding quotes" do
      context "using the full lines of the subtitle" do
        subject(:quote) { "Mas vamos lá. O período entre 1996 até 2002 foi provavelmente quando eu mais aprendi tecnologias que não são mais usadas hoje em dia. A idéia é tanto um pouco de nostalgia" }
        it { expect(srt_file.find(quote, :start_time)).to eql("00:01:40.860") }
        it { expect(srt_file.find(quote, :end_time)).to eql("00:01:51.780") }
      end

      context "using partial lines of the subtitle" do
        subject(:quote) { "e meus amigos eram todos da Engenharia Elétrica. Eu fazia Ciência da Computação e nessa época já quase não tinha nenhum amigo no meu curso mesmo." }
        it { expect(srt_file.find(quote, :start_time)).to eql("00:03:23.220") }
        it { expect(srt_file.find(quote, :end_time)).to eql("00:03:37.680") }
      end
    end
  end
end
