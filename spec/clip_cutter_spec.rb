# frozen_string_literal: true

RSpec.describe Clip::Cutter do
  let(:cutter) { Clip::Cutter.new("spec/fixtures/Episode 0032.mp4", "spec/fixtures/Episode 0032.srt", rnd_color: false) }

  it "has a version number" do
    expect(ClipCutter::VERSION).not_to be nil
  end

  describe "finding quotes" do
    # 9
    # 00:00:20,100 --> 00:00:25,769
    # vamos dizer o óbvio para com as
    #
    # 10
    # 00:00:22,890 --> 00:00:27,840
    # justificativas qualquer razão que você
    #
    # 11
    # 00:00:25,769 --> 00:00:32,130
    # está usando para não evoluir no inglês
    #
    # o problema: não necessariamente o quote vai bater com o começo e fim exato dessas linhas,
    # mas precisa encontrar do mesmo jeito
    context "using the full lines of the subtitle" do
      subject(:quote) { "vamos dizer o óbvio para com as justificativas qualquer razão que você está usando para não evoluir no inglês" }
      it "should create the ffmpeg command" do
        RSpec::Support::ObjectFormatter.default_instance.max_formatted_output_length = nil
        command1 = "ffmpeg -ss 00:00:20.100 -to 00:00:32.130 -i \"spec/fixtures/Episode 0032.mp4\" -filter:v \"crop=405:720:706:0\" -y \"/tmp/quote1_tmp.mov\""
        command2 = "ffmpeg -i /tmp/quote1_tmp.mov -vf \"drawtext=#{Clip::Cutter.joinlist(Clip::Cutter::DEFAULT_DRAWTEXT)},subtitles=/tmp/extracted.srt:force_style='#{Clip::Cutter.joinlist(Clip::Cutter::DEFAULT_SRT_STYLES, ",")}'\" -c:a copy -y /tmp/quote1_tmp2.mov"
        command3 = "ffmpeg -i /tmp/quote1_tmp2.mov -filter_complex \"[0:v]setpts=0.9*PTS[v];[0:a]atempo=1.1111111111111112[a]\" -map \"[v]\" -map \"[a]\" -y /tmp/quote1.mov"
        expect(Kernel).to receive(:system).with(command1).and_return(true)
        expect(Kernel).to receive(:system).with(command2).and_return(true)
        expect(Kernel).to receive(:system).with(command3).and_return(true)
        cutter.cut_clip(quote, "/tmp/quote1.mov")
      end
    end
  end
end
