# this is a very rough script to run after downloading all subtitles from YouTube
# to download:
#   yt-dlp "https://www.youtube.com/@akitando/videos" --get-id | \
#   xargs -I {} yt-dlp --skip-download --write-sub --sub-lang "pt-BR"
#   --no-write-auto-subs "https://www.youtube.com/watch?v={}"
# to convert vtt to srt:
#   for file in *.vtt; do ffmpeg -i "$file" -codec:s srt "${file%.vtt}.srt"; done
# then run this to fetch the titles from the akitaonrails.com homepage
# and try to match with the name of the subtitles
# there will be a couple of misses, but it's good enough for me

require 'nokogiri'

# Fetch the HTML page and extract all the post titles
html = `curl -s https://www.akitaonrails.com`
doc = Nokogiri::HTML(html)
titles = doc.css('h3.post-title').map(&:text).filter { |t| t.include?('Akitando') }
files = Dir.glob('*.srt').sort
titles.sort.each do |original_title|
  # Extract the episode number and title from the post title
  episode = original_title.scan(/#(\d+)/).flatten.first.to_i.to_s.rjust(4, '0')
  title = original_title.gsub(/\[Akitando\] #\d+ -*/, '').gsub(/\[Akitando #\d+\] -*/,'').strip
  if title.include?("|")
    title = title.split(" | ").first.strip
  end
  puts episode
  puts title
  # Rename the MP4 file with the matching post title
  files.each do |file|
    normalized = file.gsub(/[^0-9a-zA-Z ]/, '')
    if normalized.include?(title.gsub(/[^0-9a-zA-Z ]/, ''))
      puts "#{normalized} matches #{title}"
      new_file = "./episode-#{episode}.srt"
      puts "Original title: #{original_title}"
      puts "Renaming #{file} to #{new_file}"
      # Uncomment the next line to actually rename the file
      File.rename(file, new_file)
      files.delete(file) # make sure it won't be renamed again
    end
  end
  puts files.count
end

